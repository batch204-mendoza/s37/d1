const User = require ("../model/Users");

//check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result =>{

		if (result.length > 0) {
			return true
		}
		else{
			return false
		}
	})
}


// controller for user registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: reqBody.password
	})
	return newUser.save().then((user, error) =>{
		if (error){
			return false
		}
		else{
			return true
		}
	})
}