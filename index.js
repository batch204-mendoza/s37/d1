//minimum viable product. enought features that its useable

// bcrypt - for hashing 
// cors - for allowing cross - orign resourche sharting
// jsonwebtoken- for implementeing web token
//  npm init. thennpm install express mongoose cors

// to uninstall npm uninstall "package name"
// the flow goes

//models" => import to "controllers" => import to "routes" => import to "index.js"

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")
// allows access to user routes file 
const userRoutes= require("./routes/userRoutes")

const port = 4000;

const app= express();

mongoose.connect("mongodb+srv://Jack:admin123@cluster0.o4i73.mongodb.net/s37-s41?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", ()=> console.log("now connected to MongoDB Atlas!"))

app.use(cors());
app.use(express.json());

//when sendding requests= it not only goes to local host 400/users/ check email
app.use("/users", userRoutes)
app.listen(port,() =>{
	console.log(`API is now online on port ${port}`);
});



