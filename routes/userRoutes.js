const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")

// route for checking if the urser's email already exists in our database
router.post("/checkEmail", (req,res) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// routes for user registration
router.post("/register", (req,res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;