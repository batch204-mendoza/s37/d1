const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name:{
		type: String,
		// " is response if the name is not provided"
		required: [true, "course is required"]

	},
	description: {
		type: String,
		required: [true, "Description is required"]

	},
	price: {
		type: Number,
		required: [true, "Price is required"]

	},
	isActive: {
		type: Boolean,
		// so that its 
		default: true
	},
	,
	createdOn: {
		type: Date,
		default: new Date()
	}

	enrollees: [{
		userId:{
			type: String,
			required: [true,"userId is required"]
		},
		enrolledOn:{
			type: Date,
			default: new Date()
		}
	}]

})


// creates a "Course" collection in the database if  no course exists yet. but will update into the system if it already exists
module.exports = mongoose.model("Course", courseSchema);